# Shakebook - Static Site & Technical Documentation Prelude

Shakebook is a replacement prelude specifically for static site and technical documentation generation.

To get started you can use the template repository at http://gitlab.com/shakebook-template/shakebook-template

For full documentation see http://shakebook.site.
